---
title: 利用Wireshark定位QQ好友
date: 2017-06-30 16:40:50
tags: [wireshark,教程]
categories: 技术
---
根据IP定位位置原理可以参考[ip定位原理](http://blog.csdn.net/cuitang1031/article/details/52787772)

# 如何查看QQ好友地理位置

# 环境及软件：
1. Windows7
2. Wireshark
3. QQ

<!--more-->

# 步骤
1. 开启wireshark 开始包
2. 发起语言/视频聊天
3. 获取对方IP地址

    1) 选择搜索 “字符串”;

    2) 选择搜索 “分组详情”;

    3) 填写搜索数据 “020048″;
4. 查询IP地址获得地理位置信息(http://opengps.cn/Data/IP/LocHighAcc.aspx)

# 注：
本视频仅作为演示，请勿将涉及的技术于非法目的。
以下为演示视频
<video  id="video" controls="" preload="none" src="/uploads/video/findQQfriend.mp4" width="100%" poster="/uploads/video/findQQfriend_cover.png">


