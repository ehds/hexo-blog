---
title: Advanced Network Computing Test&Answer
date: 2018-11-17 16:30:48
categories: 课程
tags: 高级网络计算
mathjax: true
---

## 1  Test for SDN and NFV

1. **What are SDN and NFV?**
<!--more-->
   *There are three defination.*

   1. Software defined networks(SDN) is an approach to cloud computing that facilitates network management and enables programmatically efficient network configuration in order to improve network performance and monitoring.

   2. The physical separation of the network control plane from the forwarding plane, and where a control plane controls several devices.
   3. Network function virtualization is a network architecture concept that uses the technologies of IT virtualization to virtualize entire classes of network node functions into building blocks that may connect, or chain together, to create communication services.

2. **Difference between SDN and NFV?**

      ​SDN emphasizes more on dynamic configuration for different network functions such as switches, routers, firewalls (these services can be provided with NFV)

      ​NFV emphasizes more on the virtual implementation for network functions. 

3. **What is update consistency?**
 Update consistency denotes all network entitles are with the same version of configurations before, after and during the network update.

4. **Basic idea and benefits of two-phase update?**
      
 Two phase update: In the first phase, new version configurations/software are installed; In the second phase, packets going through the switches are tagged with version numbers.

 New version packets are handled by the new rules while old version packets are handled by the old rules.After all old version packets have been handled and delivered, the old rules will then be removed.

5. **What is congestion free update?**

​        A network update during which there are no network congestions.

6. **Describe k-reliable Traffic Engineering with Forward Fault Correction.**

​ A traffic engineering scheme that allows up to K random failures(caused by either data plane failure or control plane failure)

------



## 2  Test for NFV

1.  **What are NFV, VNF, and SFC?**

```
 NFV: Network function virtualization 
```

   > Network function virtualization is a network architecture concept that uses the technologies of IT virtualization to virtualize entire classes of network node functions into building blocks that may connect, or chain together, to create communication services.

```
  VNF: Virtual Network Function
```

   > Virtual Network Functions (VNFs) are virtualized network services running on open computing platforms formerly carried out by proprietary, dedicated hardware

      SFC: Service Function Chain

   > Service Function Chaining provides the ability to define an ordered list of a network services (e.g. firewalls, NAT, QoS). These services are then "stitched" together in the network to create a service chain

2. **Relationship between NFV and Cloud?**

   Saas maps with virtual network functions
   Paas maps with NFV orchestration and management
   Iaas maps with NFV Infrastructures

3. **Use the Misra-Gries algorithm to find out top three elements with three counters**

   (12, 14, 56, 14, 18, 14, 14, 7, 12, 14, 12, 56, 18, 17, 20, 36)

     14; 20; 36

4. **Complexity analysis for MG algorithm (worst and amortized)**

   Worst case: O (C)
   Amortized: O (1)

5. **Describe Sketch algorithm**

   Using d x w counters
   1)	Model input stream as a vector A of dimension N
   2)	Creates a small summary as an array of w x d in size
   3)	Use d hash function to map vector entries to [1…w]
   Usage:
   1)	Each entry in input vector A[ ] is mapped to one bucket per row
   2)	Estimate A[j] by taking mink{ CM[k, hk(j)] }

6. **Describe three typical ways for NFV backup**

   1. Dedicated Protection   

   ![1.png](https://i.loli.net/2018/11/17/5befd01bcafbc.png)

   2. Shared Protection

   ![1542171731617](https://i.loli.net/2018/11/17/5befd01bcb694.png)

   3. Joint protection



   ![3.png](https://i.loli.net/2018/11/17/5befd01bd65c0.png)

   ------



## 3  Test for Edge+NetBig Data

1. **Explain concept of Edge Computing？**

   ​	Edge computing is a method of optimizing cloud computing systems by taking the control of computing applications,data and services away from some central nodes(the "core“) to the other logical extreme (the "edge") of the internet which makes contact with the physical world.

   ​	Edge computing refers to the enabling technologies allowing computation to be performed at the edge of the network on downstream data on behalf of cloud services and upstream data on behalf of IoT services.

   >​	Edge computing is defined as “a method of optimizing applications or cloud-computing systems by taking some portion of an application, its data, or services away from one or more central nodes (the ‘core’) to the other logical extreme (the ‘edge’) of the Internet, which makes contact with the physical world or end users.          [The Fogging of the Cloud ](https://www.computer.org/csdl/mags/cd/2018/05/mcd2018050006.pdf)

2. **Describe two typical EC frameworks**

   ​	IoT-Edge-Cloud  provides task allocation and Offloading destination

   ​	Muti-AP Framework  provides AP selection Cooperative ,offloading Reliable and task transfer

3. **Explain the difference between EC and cloud computing**

   ​	Closer to front-end users; Less powerful than cloud; Shorter latency Higher requirement on QoE; More diverse requests(spatial-temporal diversity,wireless diversity); Mobility.

4. **What is sensorless sensing？Can you describe the general framwork for sensorless sensing？**

   ​	Sensing based on wireless signals instead of sensor hardwares.

   ![1542183466397.png](https://i.loli.net/2018/11/17/5befd0be849d5.png)

5. **Please list three typical wireless signals**

   ​	RF-signals；acoustic signals；visible light

   ------



## 4 Test for IoT

1. **Name two typical communication technologies that enables long-range low-power IoT networks**

   ​	LoRa-Long range low power communications;

   ​	NB-IoT - Narrow band IoT communications.

2. **Two types of data transfer in low power listening protocols and their suitable application scenarios**

   ​	Sender-initiated and receiver-initiated data transfer.

   ​	SI better suits network with bad links, which avoids latency casued by preamble lost; 

   ​	RI better suits network with good links, which reduces energy consumption of extra preambles.

3.  **Describe ETX,aETX and bETX**

   ​	ETX: Expected number of transmissions;

   ​	 aETX：ETX to deliver one packet to **<u>at least one</u>** of its reveivers;

   ​	bETX：ETX to deliver one packet to **<u>all</u>** of its reveivers;

4. **Explain how anycast improves energy effciency in low power IoT networks with Fig.1**

   ​	![1542114964691.png](https://i.loli.net/2018/11/17/5befd0bd264d0.png)

   <center>Fig.1</center>

      **Traditional routing**：
   $$
   \frac{1}{0.25}+1=5tx
   $$
      **Anycast** :
   $$
   \frac{1}{1-(1-0.25)^4}+1=2.5tx < 5tx
   $$

5. **Explain how link correlation affects the performance of broadcast and anycast with Fig.2**



   ![未命名文件 (6).png](https://i.loli.net/2018/11/17/5befd0e668048.png)	

   <center>Fig.2</center>

|      |      Independent       | Correlated  |
| ---- | :--------------------: | :---------: |
| aETX |        1/1 = 1         | 1/(5/8)=1.6 |
| bETX | 8+3/(5/8)+4/(4/8)=20.8 | 8/(4/8)=16  |


## Explanation

*All resources above come from course's slides, composed by [ehds](https://ehds.github.io) and karmanluo.*

*If you have some confusions and find out some mistake,please contact me(ehds@gmail.com) for correcting.*

