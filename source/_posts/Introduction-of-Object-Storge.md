---
title: Introduction of Object Storage
date: 2020-06-02 20:30:48
categories: Introduction
tags: Object Storage
mathjax: true
---

# What is object storage?

Object storage, often referred to as object-based storage, is a data  storage architecture for handling large amounts of unstructured data.  This is data that does not conform to, or cannot be organized easily  into, a traditional relational database with rows and columns. Today’s  Internet communications data is largely unstructured. This includes  email, videos, photos, web pages, audio files, sensor data, and other  types of media and web content (textual or non-textual). This content  streams continuously from social media, search engines, mobile, and  “smart” devices.

Market research firm IDC estimates that unstructured data is likely  to represent as much as 80% of all data worldwide by the year 2025.

Enterprises are finding it challenging to efficiently (and  affordably) store and manage this unprecedented volume of data.  Object-based storage has emerged as the preferred method for data  archiving and backup. It offers a level of scalability not possible with  traditional file- or block-based storage. With object-based storage,  you can store and manage data volumes on the order of terabytes (TBs),  petabytes (PBs), and even greater.

Anirup Dutta, an IBM Cloud Architect, goes over the basics in our video, "What is Object Storage?":

# Differences between block, file, and Object Storage

Main difference between block, file, and Object Storage is who accesses the data:

- Block storage
Block storage  is visible to operating systems or hypervisors that are running on a bare metal server. Operating systems write blocks of data on to disk tracks and sectors.

- File storage
File storage  is often visible directly to users in the form of a directory by way of SMB or NFS storage protocol. Users must decide and know where to store files and remember where to find them.

- Object Storage
Object Storage  is accessed directly from applications by way of RESTful API. An object is stored in a flat namespace with all other objects in the same namespace. An object name is used to write and read objects from Object Storage.Object Storage provides the capability to add custom metadata to application data.Figure 2 shows the differences between block, file, and Object Storage.

![H0@6VE_1_JQ_4I~RX`1_0_6.png](https://i.loli.net/2020/06/02/AOPG7BHMa4j9TWC.png)

*Differences between block, file, and Object Storage*



# How it works

Objects are discrete units of data that are stored in a structurally  flat data environment. There are no folders, directories, or complex  hierarchies as in a file-based system. Each object is a simple,  self-contained repository that includes the data, metadata (descriptive  information associated with an object), and a unique identifying ID  number (instead of a file name and file path). This information enables  an application to locate and access the object. You can aggregate object  storage devices into larger storage pools and distribute these storage  pools across locations. This allows for unlimited scale, as well as  improved data resiliency and disaster recovery.

Object storage removes the complexity and scalability challenges of a  hierarchical file system with folders and directories. Objects can be  stored locally, but most often reside on [cloud servers](https://www.ibm.com/cloud/learn/cloud-server), with accessibility from anywhere in the world.

Objects (data) in an object-storage system are accessed via  Application Programming Interfaces (APIs). The native API for object  storage is an HTTP-based RESTful API (also known as a RESTful Web  service). These APIs query an object’s metadata to locate the desired  object (data) via the Internet from anywhere, on any device. RESTful  APIs use HTTP commands like “PUT” or “POST” to upload an object, “GET”  to retrieve an object, and “DELETE” to remove it. (HTTP stands for  Hypertext Transfer Protocol and is the set of rules for transferring  text, graphic images, sound, video, and other multimedia files on the  Internet).

You can store any number of static files on an object storage  instance to be called by an API. Additional RESTful API standards are  emerging that go beyond creating, retrieving, updating, and deleting  objects. These allow applications to manage the object storage, its [containers](https://www.ibm.com/cloud/learn/containers), accounts, multi-tenancy, security, billing, and more.

For example, suppose you want to store all the books in a very large  library system on a single platform. You will need to store the contents  of the books (data), but also the associated information like the  author, publication date, publisher, subject, copyrights, and other  details (metadata). You could store all of this data and metadata in a  relational database, organized in folders under a hierarchy of  directories and subdirectories.

But with millions of books, the search and retrieval process will  become cumbersome and time-consuming. An object storage system works  well here since the data is static or fixed. In this example, the  contents of the book will not change. The objects (data, metadata, and  ID) are stored as “packages” in a flat structure and easily located and  retrieved with a single API call. Further, as the number of books  continues to grow, you can aggregate storage devices into larger storage  pools, and distribute these storage pools for unlimited scale.



# Benefits

There are many reasons to consider an object-storage-based solution to  store your data, particularly in this era of the Internet and digital communications that is producing large volumes of web-based, multimedia data at an increasing rate.

- Storing/managing unstructured data
- Scalability
- Disaster recovery/availability
- Customizable metadata
- Affordability
- Cloud compatibility

# Use cases

As mentioned, object-based storage is an ideal solution for storing,  archiving, backing up, and managing high volumes of static or  unstructured data.

Additional use cases include the following:

- Cloud-native applications:

Cloud-based object storage goes hand in hand with cloud application development. Build new [cloud-native](https://www.ibm.com/cloud/learn/cloud-native)
applications or transform legacy applications into next-generation 
cloud applications using cloud-based object storage as a persistent data
store. Collect and store large amounts of unstructured IoT and mobile 
data for your smart device applications. Easily and efficiently update 
your application components.

- AI and big data analytics:

Build a centralized data repository, leveraging cost-effective and scalable object storage. Collect and store unlimited amounts of data of any type, from any source. Query this data to perform big data analytics and gain valuable information about your customers, your operations and the market you serve.